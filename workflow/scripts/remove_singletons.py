import polars as pl

# Read the clustering file lazily
cluster = pl.scan_csv(snakemake.input[0], separator="\t", has_header=False, low_memory=True)

# Assume column 1 contains the cluster identifier
cluster_identifier = "column_1"

# Group by cluster identifier and count the occurrences
cluster_counts = cluster.group_by(cluster_identifier).agg([
    pl.len().alias("count")
])

# Filter out singletons (clusters with only one occurrence)
non_singletons = cluster_counts.filter(pl.col("count") > 1)

# Join back with the original data to get the filtered clusters
filtered_clusters = cluster.join(non_singletons, on=cluster_identifier)

# Collect the result
cluster = filtered_clusters.collect()

with open(snakemake.input[1]) as ids_file:
    ids = [line.strip() for line in ids_file]


# extract clusters with at least one of your sequences
with_seq = cluster.filter(pl.col("column_2").is_in(ids))["column_1"]

cluster = cluster.filter(pl.col("column_1").is_in(with_seq))

# Write the final result to a CSV file
cluster.write_csv(snakemake.output[0], separator="\t", include_header=False)

# cluster=pl.read_csv(snakemake.input[0], separator="\t", has_header=False, 
#                     low_memory=True, rechunk=False)

# with open(snakemake.input[1]) as ids_file:
#     ids = [line.strip() for line in ids_file]

# cluster=cluster.filter(pl.col("column_1").is_duplicated())

# # extract clusters with at least one of your sequences
# with_seq = cluster.filter(pl.col("column_2").is_in(ids))["column_1"]

# cluster = cluster.filter(pl.col("column_1").is_in(with_seq))

# cluster.write_csv(snakemake.output[0], separator="\t", include_header=False)
