rule mmseqs:
    input: 'results/data/proteomes/merged/{dataset}_repdb.fa.gz'
    output: temp('results/clustering/mmseqs_{dataset}_{ident}_{cov}_{mode}_cluster.tsv')
    log: 'results/log/mmseqs_{dataset}_{ident}_{cov}_{mode}.log'
    benchmark: 'results/benchmarks/mmseqs_{dataset}_{ident}_{cov}_{mode}.tsv'
    resources:
        # slurm_extra="'--qos=long"
    shell: '''
clusterdir=$(dirname {output})
mkdir -p $clusterdir

ident=$(awk "BEGIN {{print {wildcards.ident} / 100}}")
cov=$(awk "BEGIN {{print {wildcards.cov} / 100}}")

mmseqs easy-linclust {input} $clusterdir/$(basename {output} "_cluster.tsv") $TMPDIR/decon_jgi \
--min-seq-id $ident -c $cov --cov-mode {wildcards.mode} \
-e 0.001 --threads {resources.cpus_per_task} > {log}

rm $clusterdir/$(basename {output} "_cluster.tsv")_all_seqs.fasta 
rm $clusterdir/$(basename {output} "_cluster.tsv")_rep_seq.fasta
'''

# filter singletons and clusters with at least one member from the datasets
rule remove_singletons:
    input: 
        clusters=rules.mmseqs.output,
        ids='results/data/proteomes/merged/{dataset}_repdb.ids'
    output: 
        # dups=temp('results/clustering/mmseqs_{dataset}_{ident}_{cov}_{mode}_dups.ids'),
        clusters='results/clustering/mmseqs_{dataset}_{ident}_{cov}_{mode}_cluster_ns.tsv'
    threads: 50
    script: '../scripts/remove_singletons.py' #THIS WOULD BE COOL BUT IT GOES OUT OF MEM
#     shell: '''
# cont_dir=$(echo {output.dups} | sed 's/_dups.ids//')
# echo "getting the non singletons representative"
# cut -f1 {input.clusters} | uniq -d > {output.dups}

# echo "splitting the file"
# mkdir -p ${{cont_dir}}
# split {input.clusters} -n l/24 ${{cont_dir}}/chunk_

# > {output.clusters}
# for file in ${{cont_dir}}/chunk_*; do
#     echo "processing chunk $file"
#     csvtk join -H -t -f 1 "$file" {output.dups} >> {output.clusters}
#     echo "done!"
# done

# rm ${{cont_dir}}
# '''

rule get_mixed_clusters:
    input:
        clusters=rules.remove_singletons.output.clusters,
        ids='results/data/proteomes/merged/{dataset}_repdb.ids'
    output:
        mixed='results/clustering/mmseqs_{dataset}_{ident}_{cov}_{mode}_cluster_mixed.tsv'
    params: repdb=config["repdb"]
    shell: '''
cut -f1,2 {input.clusters} | awk '{{print $0"\\t"substr($2, 1, index($2, "_")-1)}}' | \
taxonkit reformat -I 3 --data-dir {params.repdb}/results/taxdump/repdb_taxdump/ -f "{{k}}" | \
awk -v dataset={wildcards.dataset} 'NR==FNR {{ ids[$1]; next }} {{ print $0 ($2 in ids ? dataset:"") }}' {input.ids} - | \
cut -f1,2,4 | csvtk uniq -H -t -f 1,3 | sort -k3,3 | csvtk fold -t -H -f 1 -v 3 -s"|" | \
 awk 'index($2, "|") {{ print }}' | awk -v dataset={wildcards.dataset} '$2!=dataset"|Eukaryota" && $2!="Eukaryota|"dataset' | \
cut -f1 > {output.mixed}.ids

cut -f1,2 {input.clusters} | awk 'NR==FNR {{ dup[$1]; next }} $1 in dup' {output.mixed}.ids - | \
awk '{{print $0"\\t"substr($2, 1, index($2, "_")-1)}}' | \
taxonkit reformat -I 3 --data-dir {params.repdb}/results/taxdump/repdb_taxdump/ | \
awk -v dataset={wildcards.dataset} 'NR==FNR {{ ids[$1]; next }} {{ print $0 ($2 in ids ? dataset:"") }}' {input.ids} - > {output.mixed}

rm {output.mixed}.ids
'''


# rule get_contaminants:
#     input: rules.get_mixed_clusters.output.mixed
#     output: "results/contamination/contaminants.txt"
#     params:
#         prop_euka=config["prop_euka"],
#         size_cluster=config["size_cluster"]
#     script: '../scripts/get_contaminants.R'

