rule download_jgi:
    params: meta=config["contsupp"]
    output: 'results/data/proteomes/jgi/{jgi}.fa.gz'
    shell: '''
accession=$(grep {wildcards.jgi} {params.meta} | cut -f7)
genome_file=$(grep {wildcards.jgi} {params.meta} | cut -f14 | rev | cut -f1 -d'/' | rev)
echo "looking for $accession: $genome_file"
bdir=$PWD
cd $TMPDIR
python3 $bdir/workflow/scripts/jgi-query.py $accession -r $genome_file
mv ${{genome_file}}.gz $bdir/{output}
cd -
'''

rule download_ncbi:
    output: 'results/data/proteomes/ncbi/{ncbi}.fa.gz'
    params: meta=config["contsupp"]
    shell: '''
accession=$(grep {wildcards.ncbi} {params.meta} | cut -f7)
datasets download genome accession $accession --include protein --filename {output}.tmp.zip
unzip -p {output}.tmp.zip ncbi_dataset/data/$accession/protein.faa | gzip > {output}    
rm {output}.tmp.zip
'''

rule download_fungidb:
    output: 
        fa='results/data/proteomes/fungidb/{fungidb}.fa.gz',
        gff='results/data/gff/fungidb/{fungidb}.gff.gz'
    shell: '''
wget "https://fungidb.org/common/downloads/release-58/{wildcards.fungidb}/fasta/data/FungiDB-58_{wildcards.fungidb}_AnnotatedProteins.fasta" \
-O /dev/stdout | gzip > {output.fa}

wget "https://fungidb.org/common/downloads/release-58/{wildcards.fungidb}/gff/data/FungiDB-58_{wildcards.fungidb}.gff" \
-O /dev/stdout | gzip > {output.gff}
'''


rule merge_jgi:
    input: 
        jgis=expand('results/data/proteomes/jgi/{jgi}.fa.gz', jgi=jgis),
        ncbis=expand('results/data/proteomes/ncbi/{ncbi}.fa.gz', ncbi=ncbis)
    output: 
        fa='results/data/proteomes/merged/jgi_repdb.fa.gz',
        ids='results/data/proteomes/merged/jgi_repdb.ids'
    params: repdb=config["repdb"]
    shell: '''
cat {params.repdb}/results/db/repdb.fa.gz {input} > {output.fa}   
seqkit seq -n -i {input} > {output.ids}
'''


rule merge_fungidb:
    input: expand('results/data/proteomes/fungidb/{fungidb}.fa.gz', fungidb=fungidbs)
    output: 
        fa='results/data/proteomes/merged/fungidb_repdb.fa.gz',
        ids='results/data/proteomes/merged/fungidb_repdb.ids'
    params: repdb=config["repdb"]
    shell: '''
cat {params.repdb}/results/db/repdb.fa.gz {input} > {output.fa}
seqkit seq -n -i {input} > {output.ids}
'''
# {params.repdb}/results/db/repdb.fa.gz


rule make_taxidmap:
    input:
        jgis=expand('results/data/proteomes/jgi/{jgi}.fa.gz', jgi=jgis),
        ncbis=expand('results/data/proteomes/ncbi/{ncbi}.fa.gz', ncbi=ncbis),
        fungidbs=expand('results/data/proteomes/fungidb/{fungidb}.fa.gz', fungidb=fungidbs)
    output: "results/data/proteomes/merged/map.ids"
    shell:'''
> {output}
for file in {input}; do
    bn=$(basename $file ".fa.gz")
    seqkit seq -n -i $file | awk -v bn=$bn '{{print $1"\\t"bn}}' >> {output}
done
'''
