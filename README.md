# Decontaminate with clustering

Benchmark mmseqs method with Olfat results or with contscout results (fungi ones)

As MMSeqs2 automatically picks the optimal clustering strategy based on the coverage mode it is smarter to play with the cov-mode parameter rather than the cluster mode.

## Contscout benchmarking

1. Download Fungal genomes of contscout
2. Cluster repdb+these genomes (checking if some species are duplicated)
3. Use different mmseqs params and see overlap with contscout results in supp table 4

## Olfat benchmarking

1. Get proteins from her same genomes
2. See if the proteins I find map to the contigs she found
3. See if you can associate a proteome to one single contaminant

# Run

```
module load hdf5 python/3.12.1 sqlite3 gcc/13.2.0 R/4.3.2 proj/9.3.1-gcc
snakemake -j 2 --ri -p -k --executor slurm --workflow-profile workflow/profiles/default/

sacct -o JobID,State,Partition,MaxVMSize,MaxRSS,NTasks,AllocCPUS,Elapsed,ReqMem
```

# TODOs

* cluster rule use slurm!!!
* Why only fungal though
* decide what sequences to cluster in RepDB
* It could be interesting to see if intraeuka contaminants have something weird
